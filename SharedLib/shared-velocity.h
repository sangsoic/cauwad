/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SHARED_VELOCITY_H
	#define SHARED_VELOCITY_H

	#include "../Core/index.h"
	#include "../Core/wave.h"
	#include "../Core/velocity.h"

	#include "../Lib/Vector/vector.h"

	void Field_apply_wave(const VField field, const size_t linearIndices, const Index rectangularIndices, const Wave wave);

#endif
