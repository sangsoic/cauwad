/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "shared-agent.h"

// MLSCRIPT-SEQ: X and Y sequences for each type of agent.

Vtype A_X(Index tail, Index head, size_t n)
{
	return 1;
}
Vtype A_Y(Index tail, Index head, size_t n)
{
	return 1;
}

Vtype B_X(Index tail, Index head, size_t n)
{
	return -(ssize_t)n;
}
Vtype B_Y(Index tail, Index head, size_t n)
{
	return -(ssize_t)n;
}

// END


void init_sequence_table(void)
{
	// MLSCRIPT-TABLE: Maps C sequence functions to an ASCII char index.
	sequenceTable['A'] = Sequence_get(A_X, A_Y);
	sequenceTable['B'] = Sequence_get(B_X, B_Y);
	// END
}

VECTOR_GET(VCell, VCell)

ACell ACell_next_agent(const ACell currentAgent, const VField field)
{
	ACell newAgent;
	// MLSCRIPT-AGENT: Determines the next position and sequence of a given agent based on its current properties and field position.

	newAgent = currentAgent;
	if (globalInterationNumber <= 13) {
		newAgent.position.i = currentAgent.position.i+5;
		newAgent.position.j = currentAgent.position.j+5;
	}
	// END
	return newAgent;
}
