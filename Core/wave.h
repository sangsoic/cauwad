/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef WAVE_H
	#define WAVE_H

	#include <stdlib.h>
	#include <stdio.h>
	#include <stdbool.h>
	#include <math.h>
	#include <stdbool.h>

	#include "index.h"

	#include "../Lib/Queue/queue.h"
	#include "../Lib/List/list.h"

	typedef double Vtype;

	struct Sequence
	{
		Vtype (* X)(Index, Index, size_t);
		Vtype (* Y)(Index, Index, size_t);
	};

	typedef struct Sequence Sequence;

	struct Wave
	{
		Index center;
		unsigned int radius;
		Sequence sequence;
	};

	typedef struct Wave Wave;

	QUEUE(Wave, Wave)

	struct QueueWave
	{
		QueueWave * waves;
		bool isEmitted;
	};

	LIST(struct QueueWave, StructQWave)

	/**
	 * \fn bool Sequence_are_equal(const Sequence a, const Sequence b)
	 * \brief Tests if 2 sequences are equal.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool Sequence_are_equal(const Sequence a, const Sequence b);

	/**
	 * \fn bool Sequence_are_equal(const Sequence a, const Sequence b)
	 * \brief Tests if 2 sequences are equal.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	Sequence Sequence_get(Vtype (* X)(Index, Index, size_t), Vtype (* Y)(Index, Index, size_t));

	/**
	 * \fn bool bool Sequence_are_null(const Sequence s)
	 * \brief Tests if given sequence is NULL
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool Sequence_are_null(const Sequence s);

	/**
	 * \fn Wave Wave_get(const Index center, const size_t radius, const Sequence sequence)
	 * \brief Allocates and initializes a wave.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	Wave Wave_get(const Index center, const size_t radius, const Sequence sequence);

	/**
	 * \fn ssize_t Wave_top_wave(const size_t j, const Wave wave)
	 * \brief Gets the top i at a given j for a given wave.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	ssize_t Wave_top_wave(const size_t j, const Wave wave);

	/**
	 * \fn ssize_t Wave_bot_wave(const size_t j, const Wave wave)
	 * \brief Gets the bottom i at a given j for a given wave.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	ssize_t Wave_bot_wave(const size_t j, const Wave wave);

	/**
	 * \fn ssize_t Wave_top_left_boundary(const Wave wave)
	 * \brief Gets top left boundary.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	ssize_t Wave_top_left_boundary(const Wave wave);
	//Same for bottom left
	ssize_t Wave_bot_left_boundary(const Wave wave, const unsigned int minRadius);
	//Same for top right
	ssize_t Wave_top_right_boundary(const Wave wave);
	//Same for bot right
	ssize_t Wave_bot_right_boundary(const Wave wave, const unsigned int minRadius);

	/**
	 * \fn unsigned int Wave_gaps_boundary(unsigned int radius)
	 * \brief Gets wave gaps.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	unsigned int Wave_gaps_boundary(unsigned int radius);

	/**
	 * \fn bool Wave_is_visible(const Wave wave, const size_t m, const size_t n)
	 * \brief Tests if wave is still visible in the field.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	bool Wave_is_visible(const Wave wave, const size_t m, const size_t n);

	/**
	 * \fn struct QueueWave StructQWave_allocinit(const Index center, const Sequence sequence)
	 * \brief Allocates and initializes \a StructQWave object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	struct QueueWave StructQWave_allocinit(const Index center, const Sequence sequence);

	/**
	 * \fn void ListStructQWave_refresh(ListStructQWave * const structQWaves, const size_t m, const size_t n)
	 * \brief Refreshes a given  \a ListStructQWave object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void ListStructQWave_refresh(ListStructQWave * const structQWaves, const size_t m, const size_t n);

	/**
	 * \fn void ListStructQWave_free(ListStructQWave * const structQWaves)
	 * \brief Frees \a ListStructQWave object.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void ListStructQWave_free(ListStructQWave * const structQWaves);

#endif
