/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "matrix.h"

VECTOR_MALLOC(VCell, VCell)

LIST_MALLOC(ACell, ACell)

LIST_MALLOC(struct QueueWave, StructQWave)

Matrix Matrix_allocate(const size_t m, const size_t n, const char * const fieldLayoutPath, FILE * * const fieldFile)
{
	Matrix mat;
	VField field;
	field.m = m;
	field.n = n;
	if (fieldLayoutPath != NULL) {
		if (Field_import_field_dimension(fieldLayoutPath, fieldFile, &field.m, &field.n)) {
			exit(EXIT_FAILURE);
		}
	}
	field.field = VectorVCell_malloc(field.m * field.n);
	mat.field = field;
	mat.agents = ListACell_malloc();
	mat.structQWaves = ListStructQWave_malloc();
	return mat;
}

Matrix Matrix_init(Matrix mat, const char * const agentsLayoutPath, const char * const fieldLayoutPath, FILE * const fieldFile)
{
	init_sequence_table();
	if (fieldLayoutPath != NULL) {
		if (Field_import_field_from(mat.field, fieldLayoutPath, fieldFile)) {
			fprintf(stderr, "*FREEING MATRIX as %s failed.*\n", __FUNCTION__);
			Matrix_free(&mat);
			exit(EXIT_FAILURE);
		}
	} else {
		Field_set_all_to_zero(mat.field);
	}
	if (agentsLayoutPath != NULL) {
		if (ListACell_init(mat.agents, mat.structQWaves, agentsLayoutPath, mat.field.m, mat.field.n)) {
			fprintf(stderr, "*FREEING MATRIX as %s failed.*\n", __FUNCTION__);
			Matrix_free(&mat);
			exit(EXIT_FAILURE);
		}
	}
}

Matrix Matrix_allocinit(const size_t m, const size_t n, const char * const agentsLayoutPath, const char * const fieldLayoutPath)
{
	Matrix mat;
	FILE * fieldFile;
	mat = Matrix_allocate(m, n, fieldLayoutPath, &fieldFile);
	Matrix_init(mat, agentsLayoutPath, fieldLayoutPath, fieldFile);
	return mat;
}

VECTOR_FREE(VCell, VCell)

LIST_FREE(ACell, ACell)

void Matrix_free(Matrix * const mat)
{
	VectorVCell_free(&mat->field.field);
	ListACell_free(&mat->agents);
	ListStructQWave_free(mat->structQWaves);
}

void Matrix_plot(const Matrix mat)
{
	Field_plot_qwaves(mat.field, mat.structQWaves, PLOT);
}

void Matrix_clear(const Matrix mat)
{
	Field_plot_qwaves(mat.field, mat.structQWaves, RESET);
}

void Matrix_display(Matrix mat, const char * const snapshotsPath, const bool printToSTDOUT, void (* Field_display)(const VField, FILE * const, const unsigned char), const unsigned char precision)
{
	bool errorOccurred;
	FILE * snapshotFile;
	char * finalSnapshotPath;
	if (snapshotsPath != NULL) {
		errorOccurred = true;
		if ((finalSnapshotPath = malloc(strlen(snapshotsPath) + (size_t)floorl(log10l(globalInterationNumber == 0 ? 1 : globalInterationNumber) + 1) + 2)) != NULL) {
			sprintf(finalSnapshotPath, "%s-%llu", snapshotsPath, globalInterationNumber);
			if ((snapshotFile = fopen(finalSnapshotPath, "w")) != NULL) {
				fprintf(snapshotFile, "[ITERATION][%llu]\n", globalInterationNumber);
				Field_display(mat.field, snapshotFile, precision);
				ListACell_print_agents_status(mat.agents, snapshotFile);
				errorOccurred = fclose(snapshotFile) == EOF;
				if (errorOccurred) {
					fprintf(stderr, "error: cannot close file '%s' due to: %s.\n", finalSnapshotPath, strerror(errno));
				}
			} else {
				fprintf(stderr, "error: cannot create file '%s' due to: %s.\n", finalSnapshotPath, strerror(errno));
			}
		} else {
			fprintf(stderr, "error: cannot allocate memory due to: %s.\n", strerror(errno));
		}
		free(finalSnapshotPath);
		if (errorOccurred) {
			fprintf(stderr, "*FREEING MATRIX as %s failed.*\n", __FUNCTION__);
			Matrix_free(&mat);
			exit(EXIT_FAILURE);
		}
	}
	if (printToSTDOUT) {
		fprintf(stdout, "[ITERATION][%llu]\n", globalInterationNumber);
		Field_display(mat.field, stdout, precision);
		ListACell_print_agents_status(mat.agents, stdout);
	}
}

void Matrix_refresh(Matrix mat)
{
	ListStructQWave_refresh(mat.structQWaves, mat.field.m, mat.field.n);
	if (ListACell_refresh(mat.agents, mat.structQWaves, mat.field)) {
		fprintf(stderr, "*FREEING MATRIX as %s failed.*\n", __FUNCTION__);
		Matrix_free(&mat);
		exit(EXIT_FAILURE);
	}
}
