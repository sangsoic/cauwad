/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "wave.h"

bool Sequence_are_equal(const Sequence a, const Sequence b)
{
	return (a.X == b.X) && (a.Y == b.Y);
}

Sequence Sequence_get(Vtype (* X)(Index, Index, size_t), Vtype (* Y)(Index, Index, size_t))
{
	Sequence s;
	s.X = X;
	s.Y = Y;
	return s;
}

bool Sequence_are_null(const Sequence s)
{
	return (s.X == NULL) || (s.Y == NULL);
}

Wave Wave_get(const Index center, const size_t radius, const Sequence sequence)
{
	Wave wave;
	wave.center = center;
	wave.radius = radius;
	wave.sequence = sequence;
	return wave;
}

ssize_t Wave_top_wave(const size_t j, const Wave wave)
{
	return floor(wave.center.i - sqrt(pow(wave.radius, 2) - pow(abs(j - wave.center.j), 2)));
}

ssize_t Wave_bot_wave(const size_t j, const Wave wave)
{
	return ceil(wave.center.i + sqrt(pow(wave.radius, 2) - pow(abs(j - wave.center.j), 2)));
}

ssize_t Wave_top_left_boundary(const Wave wave)
{
	return floor(wave.center.j - sqrt(pow(wave.radius, 2) - pow(wave.center.i, 2)));
}

ssize_t Wave_bot_left_boundary(const Wave wave, const unsigned int minRadius)
{
	return floor(wave.center.j - sqrt(pow(wave.radius, 2) - pow(minRadius, 2)));
}

ssize_t Wave_top_right_boundary(const Wave wave)
{
	return ceil(wave.center.j + sqrt(pow(wave.radius, 2) - pow(wave.center.i, 2)));
}

ssize_t Wave_bot_right_boundary(const Wave wave, const unsigned int minRadius)
{
	return ceil(wave.center.j + sqrt(pow(wave.radius, 2) - pow(minRadius, 2)));
}

unsigned int Wave_gaps_boundary(unsigned int radius)
{
	return floor(radius / sqrt(2));
}

bool Wave_is_visible(const Wave wave, const size_t m, const size_t n)
{
	size_t diff;
	return ((ssize_t)(wave.center.j - wave.radius) >= 0) || ((ssize_t)(wave.center.j + wave.radius) < n) || \
	(Wave_top_left_boundary(wave) >= 0) || (Wave_top_right_boundary(wave) < n) || \
	(Wave_bot_left_boundary(wave, diff = abs(m - 1 - wave.center.i)) >= 0) || (Wave_bot_right_boundary(wave, diff) < n);
}

QUEUE_MALLOC(Wave, Wave)

QUEUE_ENQUEUE(Wave, Wave)

static QueueWave * QueueWave_allocinit(const Index center, const Sequence sequence)
{
	QueueWave * waves;
	waves = QueueWave_malloc();
	QueueWave_enqueue(waves, Wave_get(center, 0, sequence));
	return waves;
}

struct QueueWave StructQWave_allocinit(const Index center, const Sequence sequence)
{
	struct QueueWave structQWave;
	structQWave.waves = QueueWave_allocinit(center, sequence);
	structQWave.isEmitted = true;
	return structQWave;
}

QUEUE_AT_HEAD(Wave, Wave)

QUEUE_GET_HEAD(Wave, Wave)

QUEUE_DEQUEUE(Wave, Wave)

static void StructQWave_propagate(struct QueueWave structQWave, const size_t m, const size_t n)
{
	QueueWave * queueWaves;
	QueueElementWave * currentWave;
	Wave wave;
	queueWaves = structQWave.waves;
	wave = QueueWave_get_head(queueWaves);
	if (! Wave_is_visible(wave, m, n)) {
		QueueWave_dequeue(queueWaves);
	}
	currentWave = QueueWave_at_head(queueWaves);
	while (currentWave != NULL) {
		currentWave->value->radius++;
		currentWave = currentWave->next;
	}
	if (structQWave.isEmitted) {
		wave.radius = 0;
		QueueWave_enqueue(queueWaves, wave);
	}
}

LIST_AT_HEAD(struct QueueWave, StructQWave)

LISTELEMENT_ISTAILSENTINEL(struct QueueWave, StructQWave)

LISTELEMENT_ISHEADSENTINEL(struct QueueWave, StructQWave)

LISTELEMENT_GET(struct QueueWave, StructQWave)

QUEUE_ISEMPTY(Wave, Wave)

QUEUE_FREE(Wave, Wave)

LIST_REMOVE_THIS(struct QueueWave, StructQWave)

void ListStructQWave_refresh(ListStructQWave * const structQWaves, const size_t m, const size_t n)
{
	ListElementStructQWave * currentListElementStructQWave;
	struct QueueWave currentStructQWave;
	QueueWave * waves;
	currentListElementStructQWave = ListStructQWave_at_head(structQWaves);
	while (! ListElementStructQWave_istailsentinel(currentListElementStructQWave)) {
		currentStructQWave = ListElementStructQWave_get(currentListElementStructQWave);
		StructQWave_propagate(currentStructQWave, m, n);
		if (QueueWave_isempty(waves = currentStructQWave.waves)) {
			QueueWave_free(&waves);
			ListStructQWave_remove_this(structQWaves, currentListElementStructQWave);
		}
		currentListElementStructQWave = currentListElementStructQWave->next;
	}
}

LIST_ISEMPTY(struct QueueWave, StructQWave)

LIST_POP_FRONT(struct QueueWave, StructQWave)

void ListStructQWave_free(ListStructQWave * structQWaves)
{
	ListElementStructQWave * currentListElementStructQWave;
	QueueWave * waves;
	while (! ListStructQWave_isempty(structQWaves)) {
		currentListElementStructQWave = ListStructQWave_at_head(structQWaves);
		waves = ListElementStructQWave_get(currentListElementStructQWave).waves;
		QueueWave_free(&waves);
		ListStructQWave_pop_front(structQWaves);
	}
	List_free((List * *)&structQWaves);
}
