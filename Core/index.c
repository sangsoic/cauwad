/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "index.h"

Index Index_get(const size_t i, const size_t j)
{
	Index index;
	index.i = i;
	index.j = j;
	return index;
}

Index Index_switch(const Index index)
{
	Index switched;
	switched.i = index.j;
	switched.j = index.i;
	return switched;
}

bool Index_are_equal(const Index a, const Index b)
{
	return (a.i == b.i) && (a.j == b.j);
}
