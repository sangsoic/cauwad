/* CAUWAD stands for Cellular Automata Utilizing WAve Dissipation.
 * Copyright (C) 2021  Sangsoic
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MATRIX_H
	#define MATRIX_H

	#include <stdlib.h>
	#include <stdio.h>
	#include <math.h>

	#include "index.h"
	#include "wave.h"
	#include "agent.h"
	#include "velocity.h"

	#include "../SharedLib/shared-agent.h"

	#include "../Lib/Vector/vector.h"
	#include "../Lib/Queue/queue.h"
	#include "../Lib/List/list.h"

	struct Matrix
	{
		VField field;
		ListACell * agents;
		ListStructQWave * structQWaves;
	};

	typedef struct Matrix Matrix;

	Matrix Matrix_allocinit(const size_t m, const size_t n, const char * const agentsLayoutPath, const char * const fieldLayoutPath);
	/**
	 * \fn void Matrix_free(Matrix * const mat)
	 * \brief Frees matrix.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Matrix_free(Matrix * const mat);

	/**
	 * \fn void Matrix_plot(const Matrix mat)
	 * \brief Plots matrix.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Matrix_plot(const Matrix mat);

	/**
	 * \fn void Matrix_clear(const Matrix mat)
	 * \brief Clears matrix.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Matrix_clear(const Matrix mat);

	/**
	 * \fn void Matrix_display(Matrix mat, const char * const snapshotsPath, const bool printToSTDOUT, void (* Field_display)(const VField, FILE * const, const unsigned char), const unsigned char precision)
	 * \brief Displays matrix.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Matrix_display(Matrix mat, const char * const snapshotsPath, const bool printToSTDOUT, void (* Field_display)(const VField, FILE * const, const unsigned char), const unsigned char precision);

	/**
	 * \fn void Matrix_refresh(Matrix mat)
	 * \brief Displays matrix.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 */
	void Matrix_refresh(Matrix mat);

#endif
